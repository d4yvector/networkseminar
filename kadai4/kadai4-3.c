
#include <stdio.h>

int main(int argc, char *argv[])
{
    int i1 = 0x80000000;
    int i2 = 0x40000000;
    unsigned int ui1 = 0x80000000;
    unsigned int ui2 = 0x40000000;

    printf("int\n");
    printf("%08x >> 31 = %08x\n", i1, i1 >> 31);
    printf("%08x >> 30 = %08x\n", i2, i2 >> 30);

    printf("unsigned int\n");
    printf("%08x >> 31 = %08x\n", ui1, ui1 >> 31);
    printf("%08x >> 30 = %08x\n", ui2, ui2 >> 30);

    return 0;
}
