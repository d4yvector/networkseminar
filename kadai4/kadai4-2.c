
#include <stdio.h>

int main(void)
{
    unsigned char a = 0x5a;
    unsigned char b = 0xf0;

    printf("a && b = %02x\n", (a && b));
    printf("a || b = %02x\n", (a || b));
    printf("   ! b = %02x\n", (! b));
    printf(" a & b = %02x\n", (a & b));
    printf(" a | b = %02x\n", (a | b));
    printf(" a ^ b = %02x\n", (a ^ b));
    printf("   ~ b = %02x\n", (~ b));

    return 0;
}
