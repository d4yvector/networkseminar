
#include <stdio.h>

int main(int argc, char *argv[])
{
	char a[16] = "wakayama univ.";
	char *b = "WAKAYAMA UNIV.";

	printf("a = %s\n", a);
	printf("a = %08x\n", a);
	printf("&a = %08x\n", &a);
	printf("a[0] = %08x\n", a[0]);
	printf("&a[0] = %08x\n", &a[0]);
	printf("\n");

	printf("b = %s\n", b);
	printf("b = %08x\n", b);
	printf("&b = %08x\n", &b);
	printf("b[0] = %08x\n", b[0]);
	printf("&b[0] = %08x\n", &b[0]);
	printf("\n");

	b = a;

	printf("b = %s\n", b);
	printf("b = %08x\n", b);
	printf("&b = %08x\n", &b);
	printf("b[0] = %08x\n", b[0]);
	printf("&b[0] = %08x\n", &b[0]);

	/* 終了 */
	return 0;
}
