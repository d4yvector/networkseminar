
#include <stdio.h>

#define BUFLEN 15

int main(int argc, char *argv[])
{
	unsigned char buf[BUFLEN] = "wakayama";
	unsigned char *bufp;
	int i;

	printf("Array\n");
	for (i = 0; (buf[i] != '\0') && (i < BUFLEN); i++) {
		printf("%d : %c\n", i, buf[i]);	/* ←ここは配列を使っている */
	}

	printf("\nPointer\n");
	for (bufp = &buf[0], i = 0; (*bufp != '\0') && (i < BUFLEN); bufp++, i++) {
		printf("%d : %c\n", (bufp - &buf[0]), *bufp);	/* ←ここはポインタを使っている */
	}

	/* 終了 */
	return 0;
}
